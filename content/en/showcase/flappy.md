---
title: "Flappy Bird Showcase"
anchor: "flappy_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Niashi made a [web playable game](https://niashi24.github.io/Projects/FlappyBird/index.html) with [source code](https://github.com/Niashi24/FlappyClone)

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/flappy-hat) with [source code](https://github.com/AlixBarreaux/flappy-hat)

* Gorman made a [downloadable game](https://gorman-play.itch.io/flarpy-bird) with [source code](https://github.com/GormanProg123/Flarpy-Bird)

* MrKiwi made a game with [source code](https://github.com/sosafacun/Bird-That-Flaps)

* Valumin made a [web playable game](https://valumin.itch.io/flappy-ufo)

* Xavire94 made a [web playable game](https://xavire94.itch.io/flappy-space-adventure)

* txorimalo made a [web playable game](https://txorimalo.itch.io/helloucar)

* CakeBlood made a [web playable game](https://cakeblood.itch.io/flappy-gunnar)

* Bumpi made a [web playable game](https://bumpi.itch.io/another-flappy-bird-clone)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/flappy-thingy)

* Jimothy made a [web playable game](https://gx.games/games/dyizk2/flappy-eagle/)

* Nazorus made a [web playable game](https://nazorus.itch.io/jetcave)

* AllHeart made a [web playable game](https://gx.games/games/busvgk/flying-fish/tracks/c38f865d-9823-4744-8f00-95ff8f8737a6)

* Erip  made a [web playable game](https://games.petzel.io/flappybird/index.html)

* Juan Carlos made a [web playable game](https://1juancarlos.itch.io/flappy-bird-vanilla)

* Karmanya  made a [web playable game](https://karmanya007.itch.io/elevate-a-winged-journey)

* BanMedo  made a [web playable game](https://banmedo.itch.io/fly-er)

* Boofgall made a [web playable game](https://boofgall-games.itch.io/ballon-survival)

* ochunks made a [web playable game](https://oelias.itch.io/flappy-bird-clone)

* Linkio made a [web playable game](https://linkio.itch.io/stalacflight)

* StalOlympus made a [web playable game](https://stalolympus.itch.io/flappy-birb)

* Silhouette made a [web playable game](https://thesilhouette.itch.io/flappy-bee)

* kametman made a [web playable game](https://kametman.itch.io/duely-planes)

* SanderZw made a [web playable game](https://sanderzw.itch.io/miniflappybird)

* Gaboo made a [web playable game](https://zanfers.itch.io/clickbat-web)

* yaastra made a [web playable game](https://aklesh888.itch.io/flappy-bird)

* Maddawik made a [web playable game](https://maddawik.itch.io/flap-kenney)

* ats made a game with [source code](https://github.com/asikora/ctappy-raylib)

* Sutasu made a [downloadable game](https://sutasu.itch.io/flappy-bat)

* Arnz made a [downloadable game](https://arnz.itch.io/circly-boi)

* Fictional made a [downloadable game](https://fictionalcodes.itch.io/surging-star) with [source code](https://github.com/FictionalAroma/20Games)

* Tom is Green made a [downloadable game](https://tomisgreen.itch.io/cyberflap-2084)

* trueauracoral made a [web playable game](https://trueauracoral.itch.io/flappy-bird) with a video devlog:
  {{< youtube 62iJqvonbHY >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
