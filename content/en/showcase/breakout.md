---
title: "Breakout Showcase"
anchor: "breakout_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Arakeen made a [web playable game](https://ctn-phaco.itch.io/simple-breakout) with [source code](https://github.com/AlixBarreaux/simple-breakout)

* SereneJellyfish made a [web playable game](https://serenejellyfishgames.itch.io/beachside-breakout)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/generic-breakout-clone)

* Martal made a [web playable game](https://martialis39.itch.io/brick-game)

* NotSoComfortableCouch made a [web playable game](https://notsocomfortablecouch.itch.io/breakwave)

* Jackson made a [web playable game](https://districtjackson.itch.io/break-the-breakout)

* Erip made a [web playable game](https://games.petzel.io/breakout/index.html)

* Woofenator made a [web playable game](https://woofenator.itch.io/break-in)

* Kibble made a [web playable game](https://dogfoodnight.itch.io/breakout-clone)

* Walid made a [web playable game](https://waliddib.itch.io/breakout)

* CheopisIV made a [downloadable game](https://cheopisiv.itch.io/breaker-2023)

* Cat/Jess made a [downloadable game](https://solipsisdev.itch.io/brick-break)

* Crazillo made a [web playable game](https://homefrog-games.itch.io/breakout-20-games-challenge)

* TheVerdect made a [web playable game](https://thevedect.itch.io/juicy-breakout)

* yaastra made a [web playable game](https://aklesh888.itch.io/break-out)

* storm strife made a [web playable game](https://neospare.itch.io/breakout-mini)

* Zyx750 made a [downloadable game](https://zyx750.itch.io/breakout)

* jester038 made a game with [source code available](https://github.com/kylem038/Breakout). The game is downloadable under the Releases tab.

* StalOlympus made a [web playable game](https://stalolympus.itch.io/breakout-2)

* Scholar_NZ made a [web playable game](https://scholar-nz.itch.io/breakout-by-scholar-nz)

* Xyloph made a [web playable game](https://xyloph.mooo.com:442/breakout/breakout.html) with [source code](https://github.com/Xyloph/20GC_Game2)

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/breakout):
  {{< youtube xbw1DKH0pqA >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
