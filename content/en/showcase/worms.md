---
title: "Worms"
anchor: "worms_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Greg made a [web playable game](https://an-unique-name.itch.io/almost-worms)

{{< include file="_parts/showcase_footer.md" type=page >}}
