---
title: "Super Mario Bros Showcase"
anchor: "mario_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Erip made a [web playable game](https://games.petzel.io/mario/)

* Levi Was Rigged From The Start made a [web playable game](https://boofgall-games.itch.io/super-mushroom-bro-prototype)

* Eka made a [web playable game](https://yakdoggames.itch.io/the-barroom-uprisers) with gameplay footage:
  {{< youtube 6psdATqrn-g >}}

* SDG Games made a [downloadable game](https://sdggames.itch.io/20-in-30) with a video devlog and [source code](https://gitlab.com/20-games-in-30-days/jump-man):
  {{< youtube uzu6PchSxoY >}}

{{< include file="_parts/showcase_footer.md" type=page >}}
