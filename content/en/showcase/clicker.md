---
title: "Cookie Clicker Showcase"
anchor: "clicker_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* {SDG Games} This was my third game in the challenge.
  {{< youtube CEkbyojLbJY >}}
  And here's the [source code](https://gitlab.com/20-games-in-30-days/social-clicker)

* More to come!

{{< include file="_parts/showcase_footer.md" type=page >}}
