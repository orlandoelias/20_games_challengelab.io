---
title: "Lunar Lander"
anchor: "lander"
weight: 43
---

The 1969 Apollo missions led to a number of Lunar Lander video games. Early lunar lander games were text-only, but a true graphical version (called Moonlander) appeared in 1973. Atari copied the Moonlander concept and released the Lunar Lander arcade cabinet in 1979. This game featured multiple perspectives (the camera closed in when the lander neared the moon's surface.)

Moonlander might also be the first video game to ever include an Easter Egg. If you fly horizontally fast enough, you can arrive at a McDonald's. If you land there, your astronaut will enter and get a bite to eat.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}}                          | 

### Goal:
* Create a lunar surface with uneven terrain. There should be some flat spots that act as "safe landing zones"
* Create a lunar lander. The lander will start with horizontal speed. The player can rotate the lander and thrust. Thrusting will accelerate the ship "backwards" pushing it away from the landing struts.
* Add a UI with score, a timer, and fuel. The player will get a small amount of fuel. Thrusting will burn fuel.
* Add a constant gravity. Gravity will pull the ship straight down.
* Enable collisions. If the ship lands on uneven terrain or lands too fast, it will be destroyed. If the ship lands hard, the player will get a few points. If the ship lands gently, the player will get the highest possible score.

### Stretch goal:
* You can increase the difficulty with multiple hand-crafted levels. Can you land safely on a cliff face? How about inside a winding cave? You can also tweak difficulty by manually adjusting the player's starting fuel reserves.
* Add some decorations and particle effects! (What if the rocket flame bounced off of the surface of the planet when landing? Physically simulated particle systems can do that!)

{{< expand "Showcase" >}} {{< include file="showcase/lander.md" type=page >}} {{< /expand >}}
