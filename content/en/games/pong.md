---
title: "Pong"
anchor: "pong"
weight: 10
---

Pong was the first widely successful arcade game. It was released in 1972, and started the first arcade boom. The game may seem simple today, but it was designed directly in hardware. Essentially, Pong was a specialized computer designed to bounce a ball between two paddles and keep score.

You don't have to make this from computer parts, of course! With a modern engine, this should be a fairly simple start.

| ***Difficulty*** |                          |
| :---             | :---                     |
| Complexity       | {{< icon "star-half" >}} |
| Scope            | {{< icon "star-half" >}} | 

### Goals:  
* Create an arena with two walls and a divider.
* Add a paddle on either end of the play field. Use player inputs to move the paddles up and down.
* Add a ball that moves around the playfield and bounces off of the paddles and walls.  
* Detect when the ball leaves the playfield. Assign a point to the player who scored.
* Track and display the score for each player.

### Stretch goals:
* Write an AI script that can follow the ball so you can play with only one player.  
Hint: Following the ball with a paddle is easy, but it makes the opponent impossible to beat. You might want to make the AI less than perfect somehow.
* Add a menu and allow the player to reset the game.
* Add some basic sounds. Play a sound every time the ball collides with something, and every time a player scores.

{{< expand "Showcase" >}} {{< include file="showcase/pong.md" type=page >}} {{< /expand >}}
