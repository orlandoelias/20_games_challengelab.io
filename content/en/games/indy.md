---
title: "Indy 500"
anchor: "indy"
weight: 42
---

Indy 500 was a launch title for the Atari 2600 home entertainment system in 1977. The game featured a top-down view of a race track with two cars. This is the third Indy game that Atari made, though the first two were arcade cabinets. Indy 800 was an eight player cabinet that took up 16 square feet of floor space!

Indy 500 only allowed for two players, so Atari added extra game modes to make up for it. They marketed Indy 500 as "14 games in one" with each individual track counting as a different "game mode." In reality, there were three game modes: Racing, tag, and "crash and score" - where players tried to be the first to hit a white square with their car.

| ***Difficulty*** |                                              |
| :---             | :---                                         |
| Complexity       | {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star-half" >}} |

### Goal:
* Make a race car from a top-down perspective. Cars can only turn when moving forwards or backwards. The car also should accelerate and decelerate smoothly.
* Add controls for up to two players. Player 1 should be able to race against the clock or against another player.
* Create one or more race tracks (from a top-down perspective). The track should be a complete circuit with a finish line. The original game fit the entire race track onscreen, though you can add a dynamic camera if you prefer to have larger tracks.
* Enable collisions with the track boundary and between cars.
* Add a lap/score counter.
* Add menus to select a race track, number of players, etc.

### Stretch goal:
* Add one or more additional game mode.
  * Standard racing: Players race against the clock to complete as many laps as they can, or compete to finish 25 laps (requires two players).
  * Crash and Score: (Two player) Drive into an obstacle which is randomly placed on the track. The square moves each time it is hit.
  * (Two player) This is essentially tag, whoever has the blinking car gains points by avoiding the other player's car who gains points by "tagging" the blinking car, after which the roles of the players are reversed.
* Try making a simple AI so you can play 2 player mode without a human opponent. You can use waypoints to make a "patrol route," or try something more complex.
* Why limit yourself to 2 dimensions? Add depth so you can make a figure-8 track that crosses itself with a bridge. You'll have to think about how to draw all of the layers as the cars move along the track.

{{< expand "Showcase" >}} {{< include file="showcase/indy.md" type=page >}} {{< /expand >}}
