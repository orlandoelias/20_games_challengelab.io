---
title: "The Challenge"
anchor: "games"
geekdocCollapseSection: true
weight: 30
---

Here is a list of 10 games to get you started. There are couple of alternatives, so you can select either game from each section. Each game will be slightly larger and more difficult than the game from the previous section, so you will always be learning and growing as you make more and more games.

Each game in the list below will have a short description, as well as a Definition of Done and a difficulty estimate.  
**Click/tap on the game name to see its details.**

{{< expand "Click here to see what each part of a game's description means. (example game)">}} {{< include file="_parts/example.md" type=page >}} {{< /expand >}}

### 1.
{{< tabs "game1" >}}

{{< tab "Game #1" >}}
  {{< hint type=note title="Start here" >}}
  Select a game from the tabs above. You only have to make one of the games. If you can't decide, just make Pong. After you have made this game, move on to ***"Game #2"*** below.
  {{< /hint >}}
  Step 1: Make a game!

  Gain a basic understanding of your game engine/editor and put together a complete (but small) game! <br> Don't worry about making art just yet, you can find a lot of sprites online that you can use. The main focus for now is on the mechanics of the game engine itself.

  ***Things that you will learn:***
  * Using your game engine of choice! 
  * Creating and destroying objects.
  * Processing player input and moving objects on screen.
  * Detecting and reacting to collisions.
{{< /tab >}}

{{< tab "Pong" >}}
{{< button relref="games/pong.md" >}} Game Page {{< /button >}}
{{< include file="games/pong.md" type=page  >}}
{{< /tab >}}

{{< tab "Flappy Bird" >}}
{{< button relref="games/flappy.md" >}} Game Page {{< /button >}}
{{< include file="games/flappy.md" type=page >}} 
{{< /tab >}}

{{< /tabs >}}

### 2.
{{< tabs "game2" >}}
{{< tab "Game #2" >}}

  Step 2: Make a slightly more complex game.

  You just finished your first game! Take a moment to make something else of similar scope before we move on to more complex concepts.
  
  ***Things that you will learn:***
  * Re-use code and assets between projects
  {{< hint type=tip title="You don't have to start over!" >}}
  Did you notice the similarity between Pong and Breakout, or Flappy Bird and Jetpack Joyride? You can re-use some of the code and assets that you made in the previous game.
  
  If your previous solution is too messy, then you might just want to start over. You can save yourself a lot of work if you know when (and how) to re-use existing assets in new games. Not everything is worth saving; but you don't have to always start over, either.
  {{< /hint >}}
  * Save a high score between play sessions.
  * Create and play simple sound effects (if you didn't in the previous game!)
{{< /tab >}}

{{< tab "Breakout" >}}
{{< button relref="games/breakout.md" >}} Game Page {{< /button >}}
{{< include file="games/breakout.md" type=page >}}
{{< /tab >}}

{{< tab "Jetpack Joyride" >}}
{{< button relref="games/jetpack.md" >}} Game Page {{< /button >}}
{{< include file="games/jetpack.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 3.
{{< tabs "game3" >}}
{{< tab "Game #3" >}}
  Step 3: Make some art!

  This game is similar in mechanical scope to game #2. Take the opportunity to focus on the game sprites. Try your hand at animating a moving sprite across multiple frames.  
  If you are interested in composing your own sound tracks, then this is a great time to focus on that skill as well.
  
  Finally, this is a great chance to play with particle systems. Make some particle effects that you can play when things explode (or when a frog is run over... splat!)

  ***Things that you will learn:***
  * 2D sprite art
  * Game Music
  * Particle Effects
{{< /tab >}}

{{< tab "Space Invaders" >}}
{{< button relref="games/invaders.md" >}} Game Page {{< /button >}}
{{< include file="games/invaders.md" type=page >}} {{< /tab >}}

{{< tab "Frogger" >}}
{{< button relref="games/frogger.md" >}} Game Page {{< /button >}}
{{< include file="games/frogger.md" type=page >}}
{{< /tab >}}

{{< tab "River Raid" >}}
{{< button relref="games/river_raid.md" >}} Game Page {{< /button >}}
{{< include file="games/river_raid.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 4.
{{< tabs "game4" >}}
{{< tab "Game #4" >}}

  Step 4: Make a more advanced character controller!

  Say goodbye to simple controls! This game will feature a top-down character controller with more advanced physics and multiple axis of movement.
  
  This is also a good time to start working on menus and UI design (if you haven't already). This game should have a complete main menu, as well as a game over/restart menu.

  ***Things that you will learn:***
  * 2D physics-based character controller
    * Manage 2D vectors and rotations in free space
  * Make a main menu and a restart menu.

{{< /tab >}}

{{< tab "Asteroids" >}}
{{< button relref="games/asteroids.md" >}} Game Page {{< /button >}}
{{< include file="games/asteroids.md" type=page >}}
{{< /tab >}}

{{< tab "Spacewar!" >}}
{{< button relref="games/spacewar.md" >}} Game Page {{< /button >}}
{{< include file="games/spacewar.md" type=page >}}
{{< /tab >}}

{{< tab "Indy 500" >}}
{{< button relref="games/indy.md" >}} Game Page {{< /button >}}
{{< include file="games/indy.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 5.
{{< tabs "game5" >}}
{{< tab "Game #5" >}}

  Step 5: Make an Artificial Intelligence!

  "Artificial intelligence" is an obtuse-sounding buzzword, but the fundamentals of AI aren't really that complex. For this game, you'll be teaching a computer to think.

  AI is a well developed field, so don't be afraid to do some research before diving in! There are a lot of great tools and examples to make things easier. Also, these specific games already have well-defined AI behaviors, so don't feel like you need to re-invent anything. Understanding and implementing should be enough of a challenge for now!

  ***Things that you will learn:***
  * Make a basic AI.

{{< /tab >}}

{{< tab "Pac Man" >}}
{{< button relref="games/pacman.md" >}} Game Page {{< /button >}}
{{< include file="games/pacman.md" type=page >}}
{{< /tab >}}

{{< tab "Tic-Tac-Toe" >}}
{{< button relref="games/tic_tac_toe.md" >}} Game Page {{< /button >}}
{{< include file="games/tic_tac_toe.md" type=page >}}
{{< /tab >}}

{{< tab "Conway's Game of Life" >}}
{{< button relref="games/life.md" >}} Game Page {{< /button >}}
{{< include file="games/life.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 6.
{{< tabs "game6" >}}
{{< tab "Game #6" >}}

  Step 6: Make a simple platformer!

  It's finally time to leave the arcade. Make a 2D character platformer. Specifically, use tile sets (or something similar) to build a world from small pieces. Create multiple enemies and obstacles, and make a few levels.

  Make sure to practice everything from the previous games: You should be able to make sounds, sprites, and game logic at this point.

  ***Things that you will learn:***
  * Platformer character controller
  * Draw and use 2D tile sets. Create large levels from a simple palette. 
  * Create moving platforms and enemies, set up basic rooms. The player will move/warp between different areas, so loading and unloading might be necessary.

{{< /tab >}}

{{< tab "Super Mario Bros." >}}
{{< button relref="games/mario.md" >}} Game Page {{< /button >}}
{{< include file="games/mario.md" type=page >}}
{{< /tab >}}

{{< tab "Pitfall" >}}
{{< button relref="games/pitfall.md" >}} Game Page {{< /button >}}
{{< include file="games/pitfall.md" type=page >}}
{{< /tab >}}

{{< tab "VVVVVV" >}}
{{< button relref="games/vvvvvv.md" >}} Game Page {{< /button >}}
{{< include file="games/vvvvvv.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 7.
{{< tabs "game7" >}}
{{< tab "Game #7" >}}

  Step 7: Make a more complex 2D game with modifiable terrain.

  ***Things that you will learn:***
  * Edit the world terrain in realtime.

{{< /tab >}}

{{< tab "Worms" >}}
{{< button relref="games/worms.md" >}} Game Page {{< /button >}}
{{< include file="games/worms.md" type=page >}}
{{< /tab >}}

{{< tab "Dig Dug" >}}
{{< button relref="games/dig_dug.md" >}} Game Page {{< /button >}}
{{< include file="games/dig_dug.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 8.
{{< tabs "game8" >}}
{{< tab "Game #8" >}}

  Step 9: Let's enter the 3rd dimension!
  
  ***Things that you will learn:***
  * Introduction to 3D
  * 2.5D game mechanics (moving in 2 dimensions in a 3D world)
  * Basic 3D modeling.

{{< /tab >}}

{{< tab "Super Monkey Ball" >}}
{{< button relref="games/monkeyball.md" >}} Game Page {{< /button >}}
{{< include file="games/monkeyball.md" type=page >}}
{{< /tab >}}

{{< tab "Star Fox" >}}
{{< button relref="games/star_fox.md" >}} Game Page {{< /button >}}
{{< include file="games/star_fox.md" type=page >}}
{{< /tab >}}

{{< tab "Crash Bandicoot" >}}
{{< button relref="games/crash.md" >}} Game Page {{< /button >}}
{{< include file="games/crash.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 9.
{{< tabs "game9" >}}
{{< tab "Game #9" >}}
  
  Step 9: Make a fully 3D game!

  ***Things that you will learn:***
  * Full 3D game
  * Quaternion Rotation
  * First-Person controller

{{< /tab >}}

{{< tab "Wolfenstein or Doom" >}}
{{< button relref="games/doom.md" >}} Game Page {{< /button >}}
{{< include file="games/doom.md" type=page >}}
{{< /tab >}}

{{< tab "Mario Kart" >}}
{{< button relref="games/mario_kart.md" >}} Game Page {{< /button >}}
{{< include file="games/mario_kart.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

### 10.
{{< tabs "game10" >}}
{{< tab "Game #10" >}}

  Step 10: Make a 3D game with an advanced concept!
  
  ***Things that you will learn:***
  * Pick between one of the following:
    * Make an infinite, procedural voxel world!
    * Create working portals with a shader and custom physics handling.
  
{{< /tab >}}

{{< tab "Minecraft" >}}
{{< button relref="games/minecraft.md" >}} Game Page {{< /button >}}
{{< include file="games/minecraft.md" type=page >}}
{{< /tab >}}

{{< tab "Portal" >}}
{{< button relref="games/portal.md" >}} Game Page {{< /button >}}
{{< include file="games/portal.md" type=page >}}
{{< /tab >}}

{{< /tabs >}}

## Game 11 and Beyond

Congratulations on completing your 10th game! From this point onwards, the list is in your hands. You know what type of game you're interested in making. Try making games that teach you the specific skills you will need. For example, if you want to make real-time strategy games, then you could learn a lot from tower defense games. Take a look at the [full list]({{< ref "games/_index.md" >}}) (but feel free to deviate if there's a different game you want to try.)
